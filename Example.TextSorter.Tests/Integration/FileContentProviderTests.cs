﻿
using System.IO;
using NUnit.Framework;

namespace Example.TextSorter.Tests.Integration
{
    [TestFixture]
    public class FileContentProviderTests
    {
        private readonly string _fileName = $"{nameof(FileContentProviderTests)}TextFile.txt";

        private readonly string _fileContent = "here is the content";

        [OneTimeSetUp]
        public void Setup()
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }

            File.WriteAllText(_fileName, _fileContent);
        }

        [OneTimeTearDown]
        public void Teardown()
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }
        }

        [Test]
        public void Given_FileExists_StringContentReturned()
        {

            var subject = new FileContentProvider();

            var content = subject.LoadContent(_fileName);

            Assert.AreEqual(_fileContent, content);

        }


        [Test]
        public void Given_InvalidFileName_EmptyStringReturned()
        {
            var subject = new FileContentProvider();

            var content = subject.LoadContent("unknown.txt");

            Assert.IsEmpty(content);

        }

    }
}
