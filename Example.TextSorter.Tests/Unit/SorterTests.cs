﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Example.TextSorter.Tests.Unit
{

    [TestFixture]
    public class SorterTests
    {
        
        [TestCaseSource(typeof(SorterTestData), nameof(SorterTestData.Data))]
        public void Given_StringOfText_Process_ReturnsResult(string text, Dictionary<string, int> expectedResult)
        {
            
            var subject = new Sorter();
            
            var actualResult = subject.Sort(text);

            foreach (var d in actualResult)
            {
                Console.WriteLine($"{d.Key} - {d.Value}");
            }

            Assert.AreEqual(actualResult, expectedResult);

        }
        
    }
}
