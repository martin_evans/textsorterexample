﻿using System.Collections.Generic;

namespace Example.TextSorter.Tests.Unit
{
    public static class SorterTestData
    {

        public static object[] Data =
        {
            new object[] { "Go do that thing that you do so well", new Dictionary<string, int>()
            {
                {"do", 2},
                {"go", 1},
                {"that", 2},
                {"thing", 1},
                {"you", 1},
                {"so", 1},
                {"well", 1},
            }},
            new object[] { "Fred Sally Grace Lilly Sam Sally Grace Dave Sally", new Dictionary<string, int>()
            {
                {"fred", 1},
                {"sally", 3},
                {"grace", 2},
                {"lilly", 1},
                {"sam", 1},
                {"dave", 1}
            }},
            new object[] { "Although this contains a comma, this should not cause an issue", new Dictionary<string, int>()
            {
                {"although", 1},
                {"this", 2},
                {"contains", 1},
                {"a", 1},
                {"comma", 1},
                {"should", 1},
                {"not", 1},
                {"cause", 1},
                {"an", 1},
                {"issue", 1}
            }},
            new object[] { "Although this contains a comma, and other characters, this should not cause an issue.", new Dictionary<string, int>()
            {
                {"although", 1},
                {"this", 2},
                {"contains", 1},
                {"a", 1},
                {"comma", 1},
                {"should", 1},
                {"not", 1},
                {"cause", 1},
                {"an", 1},
                {"and", 1},
                {"other", 1},
                {"characters", 1},
                {"issue", 1}
            }},
            new object[] { @"This 
file contains 


a couple of carriage returns.", new Dictionary<string, int>()
            {
                {"this", 1},
                {"file", 1},
                {"contains", 1},
                {"a", 1},
                {"couple", 1},
                {"of", 1},
                {"carriage", 1},
                {"returns", 1}
            }},


        };

        
    }
}