﻿using System.IO;

namespace Example.TextSorter
{
    
    public class FileContentProvider
    {
        public string LoadContent(string path)
        {
            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}