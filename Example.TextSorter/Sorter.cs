﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Example.TextSorter
{
    public class Sorter
    {
        public Dictionary<string, int> Sort(string text)
        {

            var groupedText = text
                .ToLower()
                .Split(new[] {" ",",",".","\n", "\r"}, StringSplitOptions.RemoveEmptyEntries)
                .GroupBy(x => x)
                .Select(x => new {word = x.Key, count = x.Count()});

            return groupedText.ToDictionary(wordAndCount => wordAndCount.word, wordAndCount => wordAndCount.count);

        }
    }
}