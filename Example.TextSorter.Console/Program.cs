﻿using System;

namespace Example.TextSorter.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                try
                {
                    var fc = new FileContentProvider();

                    var content = fc.LoadContent(args[0]);

                    if (content != string.Empty)
                    {
                        var f = new Sorter();

                        var ret = f.Sort(content);

                        foreach (var keyValuePair in ret)
                        {
                            System.Console.WriteLine($"{keyValuePair.Value} : {keyValuePair.Key}");
                        }
                        
                    }
                    
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e);
                }
               
            }

            System.Console.ReadLine();

        }
    }

    
}
